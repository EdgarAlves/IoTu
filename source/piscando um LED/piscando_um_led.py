#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  	piscando_um_led.py
#
#  	Programa para piscar um LED conectado ao pino GPIO 18
#	
#  	Autor: Edgar Alves
#  	2017
#  
#	Baseado no projeto de Ken Powers - 2015
#	Todos os direitos reservados


#Importa as bibliotecas necessarias
import time
import RPi.GPIO as GPIO	#Renomeia a biblioteca RPi.GPIO para GPIO a fim de facilitar o uso

#Define o sistema de numeracao da GPIO como BCM (Broadcom System On Chip), 
#ou seja, nao usa a numeracao correspondente a ordem fisica dos pinos
GPIO.setmode(GPIO.BCM)

#Constantes
RUNNING = True
led = 18

#Define o pino do "led" como saída na GPIO
GPIO.setup(led, GPIO.OUT)

print("Iniciada a rotina de piscar o LED!")
print("Pressione CTRL + C para sair")


#Rotina de piscar o LED
try:
	while (RUNNING):
		#Acende o LED e espera 0,5 segundos
		GPIO.output(led, GPIO.HIGH)
		time.sleep(0.5)
		
		#Apaga o LED e espera 0,5 segundos
		GPIO.output(led, GPIO.LOW)
		time.sleep(0.5)

#Se CTRL+C for pressionado o loop principal é parado
except KeyboardInterrupt:
	RUNNING = False
	print("\nSaindo")

#O campo finally sempre é executado
finally:
	#Limpa a configuração da GPIO ao terminar o programa
	GPIO.cleanup()
	#Assim os pinos estarao disponiveis para ser usados novamente

#FIM
